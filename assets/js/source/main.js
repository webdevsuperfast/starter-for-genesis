(function($) {
	"use strict";

	$(document).ready(function(){
		// Add class to images with anchor links
		$('.content').find('img').parent('a').addClass('imgwrap');

		// Smooth Go to Top
		$('body').append('<div class="gototop"><span class="mega-octicon octicon-chevron-up"></span></div>');

		var gototop = $('.gototop, .gotobottom');
		$(window).scroll(function(){
			if ($(this).scrollTop() > 200 ){
				gototop.fadeIn(500);
				gototop.addClass('top');
			} else {
				gototop.fadeOut(500);
				gototop.removeClass('top');
			}
		});

         $('.gototop').click(function () {
             $('html, body').animate({
                 scrollTop: '0px'
             }, 500);
             return false;
         });

		// Responsive Menu
		$( '.genesis-nav-menu' ).before( '<button class="menu-toggle" role="button" aria-pressed="false"></button>' ); // Add toggles to menus
		$( '.genesis-nav-menu .sub-menu' ).before( '<button class="sub-menu-toggle" role="button" aria-pressed="false"></button>' ); // Add toggles to sub menus

		// Show/hide the navigation
		$( '.menu-toggle, .sub-menu-toggle' ).on( 'click', function() {
			var $this = $( this );
			$this.attr( 'aria-pressed', function( index, value ) {
				return 'false' === value ? 'true' : 'false';
			});

			$this.toggleClass( 'activated' );
			$this.next( '.genesis-nav-menu, .sub-menu' ).slideToggle( 'fast' );

		});

		// FitVids
		var container = $( '.video, .content' );
		container.fitVids();
		container.fitVids( {
			customSelector: "iframe[src*='//fast.wistia.net']"
		} );
		$('.content p').each(function() {
			var $this = $(this);
			if($this.html().replace(/\s| /g, '').length === 0){
				$this.addClass('empty');
			}
		});
	});

	// Window load event with minimum delay
	// @https://css-tricks.com/snippets/jquery/window-load-event-with-minimum-delay/
	(function fn() {
		fn.now = +new Date;
		$(window).load(function() {
			if (+new Date - fn.now < 100) {
				setTimeout(fn, 100);
			}
			// var foo = $('body');
			// $('body').addClass('animated fadeIn');
			// $('body').stop().fadeIn(300);
			/* foo.velocity('fadeIn', {
					duration: 500
			}); */
            // foo.css('visibility', 'visible');
		});
	})();
})(jQuery);
