<?php
/**
 * Front Page
 *
 * @package      Boilerplate for Genesis
 * @since        1.0
 * @link         http://www.superfastbusiness.com
 * @author       SuperFastBusiness <www.superfastbusiness.com>
 * @copyright    Copyright (c) 2014, SuperFastBusiness
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
*/

// Force full-width-content layout setting
// add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

genesis();
