<?php
/**
 * Widget Areas
 *
 * @package      Boilerplate for Genesis
 * @since        1.0
 * @link         http://www.superfastbusiness.com
 * @author       SuperFastBusiness <www.superfastbusiness.com>
 * @copyright    Copyright (c) 2014, SuperFastBusiness
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
*/

// Register Sidebar Function
function mb_register_sidebars() {
	// Register Custom Sidebars
    genesis_register_sidebar( array(
        'id' => 'footer-right',
        'name' => __( 'Footer Right', 'starter' ),
        'description' => __( 'This is the footer right area. It usually appears after the copyright information.', 'starter' )
    ) );
}
